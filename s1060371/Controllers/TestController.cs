﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace s1060371.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "123";
            ViewBag.Name = "vita";

            ViewData["A"] = 1;
            ViewData["B"] = 2;

            ViewBag.A = 1;
            ViewBag.B = 2;

            return View();
        }
        public ActionResult html()
        {
            return View();
        }
        public ActionResult htmlhelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}